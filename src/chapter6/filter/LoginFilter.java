package chapter6.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import chapter6.beans.User;

@WebFilter(urlPatterns= {"/setting", "/edit"})
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse)response;
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("loginUser");
        String errorMessages = "ログインしてください";


		if (user != null) {
			chain.doFilter(request, response); // サーブレットを実行
		} else if (user == null && req.getServletPath().equals("/login")) {
            res.sendRedirect("./");
			return;
		} else {
            session.setAttribute("errorMessages", errorMessages);
            res.sendRedirect("./");
			return;
		}
	}

	@Override
	public void init(FilterConfig config) {
	}

	@Override
	public void destroy() {
	}

}
