package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //編集のものをアップデート
	public void update(Message messageEdit ) {
		Connection connection = null;

        try{
        	connection = getConnection();
        	new MessageDao().update(connection, messageEdit);
        	commit(connection);

        } catch (RuntimeException e) {
        	rollback(connection);
        	throw e;
        } catch (Error e) {
        	rollback(connection);
        	throw e;
        } finally {
        	close(connection);
        }
    }

    /*
     * selectの引数にString型のuserIdを追加
     */
	public List<UserMessage> select(String userId, String startDate, String endDate) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		String start = null;
		String end = null;

		if (!StringUtils.isEmpty(startDate)) {
			start = startDate + " 00:00:00";
		} else {
			String startTime = "2021-05-06 00:00:00";
			start = startTime;
		}

		if (!StringUtils.isEmpty(endDate)) {
			end = endDate + " 23:59:59";
		} else {
			Date endTime = new Date();
			SimpleDateFormat endTimesdf = new SimpleDateFormat("yyyy-MM-dd 23:59:59");
			end = endTimesdf.format(endTime);
		}


		try {
			connection = getConnection();

			Integer id = null;
			if(!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, start, end);

			commit(connection);
			return messages;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public Message selectMessage(int id) {
		Connection connection = null;
        try {
        	connection = getConnection();
            Message message = new MessageDao().select(connection, id);
            commit(connection);

            return message;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //つぶやきの削除
	public void delete(int id) {
    	Connection connection = null;

    	try {
    		connection = getConnection();
    		new MessageDao().delete(connection, id);
    		commit(connection);


        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);

    	}
    }
}